package wordTree.driver;
import wordTree.util.FileProcessor;
import wordTree.threadMgmt.Treebuilder;
import wordTree.threadMgmt.CreateWorkers;
import wordTree.store.Results;
import wordTree.util.MyLogger;
//import airportSecurityState.airportStates.Context;
import java.lang.System;
import java.io.IOException;


/** Starts the multithreaded wordTree program
@author lamar arias 
@author	cameron parlman
@version 1.0 
@since 0.0
*/
public class Driver{
 

//run command
//ant -buildfile src/build.xml run -Dargs='src/input.txt src/output.txt 1 fox 0'
 public static void main(String [] args){
		String inputName="";
		String outputName="";
		int numThreads =1;
		int debugLevel = 1;

		/*	VALIDATION: command line  
			catch incorrect number of command line arguments 	
		*/
		if(args.length < 5){
			usage(); 
			System.exit(1);
		}

		/*	ASSIGNMENTS: default
			assign input and output names 
			try to assign numThreads and debugLevel. 
		*/
		inputName = args[0];	
		outputName = args[1];	
		try{
			numThreads = Integer.parseInt(args[2]);//number of threads
			debugLevel = Integer.parseInt(args[args.length-1]);//debug level
		}catch(NumberFormatException b){
				System.err.println("number not given for number of threads or  debug level.");//TODO
				b.printStackTrace();
				System.exit(1);
		}

		//VALIDATION check numThreads & DebugVal in range
		if(numThreads <1 || numThreads > 3){System.err.println("Number of Threads out of range (1-3)");}
		if(debugLevel <0 || debugLevel > 4){System.err.println("DebugLevel out of range (0-4)");}


		//VALIDATION: use if num thread needs to be same as number of delete words 
		//also assuming that debug val always included in command line args 
		String[] delWords = args[3].split("[,]+");
		if(delWords.length != numThreads){
			System.err.println("#Delete words != numThreads\n");//TODO
			System.exit(1);
		}
		/*END VALIDATION:*/
		
		
		/* 	create Declare Treebuilder, FileProcessor, Results 
			set debug level 
			pass input file to Fileprocessor 	
		*/
		MyLogger.setDebugValue(Integer.parseInt(args[4]));
		Treebuilder tree = new Treebuilder();
		FileProcessor inp = new FileProcessor();
		Results out = new Results();
		inp.op(args[0]);


		/* 
			Create thread Manager CreateWorkers then and call methods that
			start the threads
		*/
		CreateWorkers creator = new CreateWorkers(inp, out, tree,args[1]);
		creator.startPopulateWorkers(Integer.parseInt(args[2]));
		creator.startDeleteWorkers(Integer.parseInt(args[2]), args[3]);


		/*
			write results to file 
		*/
		out.writeToFile(outputName);	 	


		/*
			close input file.		
		*/
		inp.cl();
 
	}/* END MAIN*/



	/** Display print the command line usage 
	*/
	public static void usage(){
		System.err.println("wordTree input output 1 banana 0");
	} 
 
}



/* 
	COMMENTED CODE 
*/

 //run command ant -buildfile src/build.xml run -Darg0=src/input.txt -Darg1=src/output.txt -Darg2=3 -Darg3=a,as,fox -Darg4=0

//String wdelim = "[,]+";
		//String ldelim = "[ ]+";
		//String [] wds = args[3].split(wdelim); 


/*
 	String line;
 	while((line = inp.re()) != null){
 		String [] w = line.split(ldelim);
 		for(int i = 0; i < w.length; i++){
 			tree.insertS(w[i]);
 		}
 	}
 	*/
 	
//creator.out = tree.printNodes(creator.out, tree.root);
//creator.out.writeToFile(args[1]);
