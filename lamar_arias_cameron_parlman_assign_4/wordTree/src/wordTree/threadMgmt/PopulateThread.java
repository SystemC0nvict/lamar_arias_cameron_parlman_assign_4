package wordTree.threadMgmt;

import wordTree.threadMgmt.Treebuilder;
import wordTree.util.FileProcessor;
import wordTree.store.Results;
import wordTree.util.MyLogger;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/** Threaded class, that reads a line from a file and inserts it into a tree.
*/
public class PopulateThread implements Runnable{
	public FileProcessor fi;
	public Treebuilder tree;

	/**
		Constructor
		@param fileprocessor_in FileProcessor object that reads files 	
		@param treebuilder_in TreeBuilder object that builds trees
	*/
	public PopulateThread(FileProcessor fileprocessor_in, Treebuilder treebuilder_in){
		MyLogger.writeMessage("PopulateThread(FileProcessor, Treebuilder)", MyLogger.DebugLevel.CONSTRUCTOR);
		fi = fileprocessor_in;
		tree = treebuilder_in;
	}
	

	/** threaded method. 
		reads file line by line and inserts each word from the line into the tree. 
	*/	
	public void run(){
		String line;
		while((line = fi.re()) != null){
		//MyLogger.writeMessage(line, MyLogger.DebugLevel.CHECKW);
			String [] w = parseline(line);
			for(int i = 0; i < w.length; i++){
 				tree.insertS(w[i]);
				
 			}
		}
		MyLogger.writeMessage("populate thread made", MyLogger.DebugLevel.RUN);
	}
	

	/** splits a line into a string array of words 
	@param line a line to split by spaces.
	@return a string array of words 
	*/
	public String [] parseline(String line){
		String ldelim = "[ ]+";
		String [] w = line.split(ldelim);
		
		return w;
		
	}

}
