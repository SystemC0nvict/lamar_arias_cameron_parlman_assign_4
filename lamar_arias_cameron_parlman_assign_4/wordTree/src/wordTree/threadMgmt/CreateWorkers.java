package wordTree.threadMgmt;
import wordTree.threadMgmt.PopulateThread;
import wordTree.threadMgmt.DeleteThread;
import wordTree.threadMgmt.Treebuilder;
import wordTree.util.FileProcessor;
import wordTree.store.Results;
import java.util.ArrayList;
import java.lang.System;
import java.io.IOException;
import wordTree.util.MyLogger;


/** Thread manager, controls the insertion and deletion of words in a tree.
	creates PopulateThreads and DeleteThreads 	
		
*/
public class CreateWorkers {
	public FileProcessor fi;
	public Results out;
	public Treebuilder tree;
	public String outname;
	public ArrayList<Thread> pthreads;
	public ArrayList<Thread> dthreads;
	

	/** Constructor 
		@param fileobj 		fileprocessor object, reads from file
		@param resultsobj	Results object, stores results for display
		@param tr			Treebuilder object, builders a tree 
		@param name			the output file name 
	
	*/
	public CreateWorkers(FileProcessor fileobj, Results resultsobj, Treebuilder tr, String name){
		MyLogger.writeMessage("CreateWorkers(FileProcessor, Results, Treebuilder, String", MyLogger.DebugLevel.CONSTRUCTOR);
		fi = fileobj;
		out = resultsobj;
		tree = tr;
		outname = name;
		pthreads = new ArrayList<Thread>();
		dthreads = new ArrayList<Thread>();
	}
	

	/** creates PopulateThread workers to insert words into a tree 
		@param number the number of PopulateThreads to create 
	*/
	public void startPopulateWorkers(int number){
		
		for(int i = 0; i < number; i++){
			PopulateThread tmp = new PopulateThread(fi, tree);
			Thread thr = new Thread(tmp);
			pthreads.add(thr);
			thr.start();
			
		}
		try{
			for(int b = 0; b < pthreads.size(); b++){
				pthreads.get(b).join();
			}
		}catch (InterruptedException ie) {
			System.err.println("Error joining threads");	
			ie.printStackTrace();
			System.exit(1);
		}
       		fi.cl();
       		out = tree.writeResults(out, tree.root);
	}


	/** creates DeleteThread  workers to delete words from a tree 	
		@param number the number of DeleteThreads to create 
		@param delws the String of words to delete 
	*/
	public void startDeleteWorkers(int number, String delws){
		String [] wds = getdels(delws);
		for(int i = 0; i < number; i++){
			DeleteThread tmp = new DeleteThread(tree, wds[i]);
			Thread thr = new Thread(tmp);
			dthreads.add(thr);
			thr.start();
		}
		try{
			for(int b = 0; b < dthreads.size(); b++){
				dthreads.get(b).join();
			}
		}catch (InterruptedException ie) {
			System.err.println("Error joining threads");	
			ie.printStackTrace();
			System.exit(1);
		}
       		out = tree.writeResults(out, tree.root);
	}

	
	/** takes a string and splits it by commas 
		@param dels a string of comma separated words to split 	
		@return a string array of comma separated words 
	*/
	public String [] getdels(String dels){
		String wdelim = "[,]+";
		String [] wds = dels.split(wdelim);
		return wds;
		
		
	}
	
	
	
}
