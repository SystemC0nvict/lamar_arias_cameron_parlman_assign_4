package wordTree.threadMgmt;
import wordTree.threadMgmt.Treebuilder;
import wordTree.util.MyLogger;

/** DeleteThread a thread object that deletes words from a tree
*/
public class DeleteThread implements Runnable{
	public Treebuilder tree;
	public String del;
	
	/** Constructor 
		@param tr Treebuilder object, contains the tree. 	
		@param wd the name of the word to be deleted from the tree.
	*/
	public DeleteThread(Treebuilder tr, String wd){
		MyLogger.writeMessage("DeleteThread(Treebuilder, String)", MyLogger.DebugLevel.CONSTRUCTOR);
		tree = tr;
		del = wd;
	}
		
	
	/** threaded function start. 
		deletes a word from the tree 
	*/
	public void run(){
		tree.tmpDel(tree.root, del);
		MyLogger.writeMessage("delete thread made", MyLogger.DebugLevel.RUN);
	}


};
