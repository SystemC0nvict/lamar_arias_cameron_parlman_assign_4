package wordTree.test;

import wordTree.threadMgmt.Node;
import wordTree.threadMgmt.PopulateThread;
import wordTree.threadMgmt.DeleteThread;
import wordTree.threadMgmt.Treebuilder;
import wordTree.util.FileProcessor;
import wordTree.store.Results;
import java.util.ArrayList;
import java.lang.System;
import java.io.IOException;




public class test{
	public static void main(String[] args){
			
		/*TEST 1
			Nodes tests
		*/
	
		/*TEST 1.A
			test Node constructor
		*/	
		Node node1 = new Node("TEST");
		if(! node1.getWord().equals("TEST")){
			System.out.println("Test:1.A: Failed expected:\"TEST\" but was " + node1.getWord());	
		}
		else{System.out.println("Test:1.A: Passed");}


		/*TEST 1.B
			test Node setWord()	
		*/
		node1.setWord("hello");
		if(! node1.getWord().equals("hello")){
			System.out.println("Test:1.B: Failed expected:\"hello\" but was " + node1.getWord());	
		}
		else{System.out.println("Test:1.B: Passed");}


		/*TEST 1.C
			test Node setWord()	
		*/
		Node node2 = new Node("world");
		node1.setRight(node2);
		if(node1.getRight() != null){
			if(! node1.getRight().getWord().equals("world")){
				System.out.println("Test:1.C: Failed expected:\"world\" but was " + node2.getWord());	
			}
			else{System.out.println("Test:1.C: Passed");}
		}
		else{
			System.out.println("Test:1.C: Failed node1 right child was null");
		}

		
		/*TEST 1.D
			test charcount after setWord();	
		*/	
		node1.setWord("testword");
		int count1 = node1.getCharCount();
		if(	count1 != 8){
			System.out.println("Test:1.D: Failed expected:\"8\" but was " + count1);	
		}
		else{
			System.out.println("Test:1.D: Passed");}
	





		/*TEST 2   
		Treebuilder BST insertion, deletion, lexicographical ordering
		*/
		Treebuilder tree1 = new Treebuilder();			
		tree1.insertS("TEST");
		
		/*TEST:2.A
			Insert a word 
		*/		
		if(tree1.countW(tree1.getRoot()) != 1 ){
			System.out.println("Test:2.A: Failed expected:1: but was "+tree1.countW(tree1.getRoot())+":");
		}
		else{
			System.out.println("Test:2.A: Passed");	
		}


		/*TEST:2.B
			Delete the one word.		
		*/		
		tree1.delete("TEST");
		if(tree1.countW(tree1.getRoot()) != 0 ){
			System.out.println("Test:2.B: Failed expected:0: but was "+tree1.countW(tree1.getRoot())+":");
		}
		else{
			System.out.println("Test:2.B: Passed");	
		}
	
	
		/*TEST:2.C 
			test lexicographical insert order. on left child
		*/
		tree1.insertS("Hello");
		Node left = tree1.getRoot().getLeft();	
		if(left != null){
			if(! left.getWord().equals("Hello")){
				System.out.println("Test:2.C: Failed expected:Hello: but was "+left.getWord()+":");
			}
			else{
				System.out.println("Test:2.C: Passed");	
			}
		}
		else{
			System.out.println("Test:2.C:Failed left child was Null");
		}

		/*TEST:2.D
			test lexicographical insert order. on right child 
		*/
		tree1.insertS("world");
		Node right = tree1.getRoot().getRight();	
		if(right != null){
			if(! right.getWord().equals("world")){
				System.out.println("Test:2.D: Failed expected:world: but was "+right.getWord()+":");
			}
			else{
				System.out.println("Test:2.D: Passed");	
			}
		}
		else{
			System.out.println("Test:2.D:Failed right child was Null");
		}


		/*TEST 3*/
		/*TEST3.A  number of distinct words 
		*/
		tree1.insertS("TEST");
		tree1.insertS("TEST");
		int count = tree1.countDW(tree1.getRoot());
		if(count != 3){
			System.out.println("Test:3.A: Failed, expected:3: but was "+count+":");	
		}	
		else{System.out.println("Test:3.A: Passed");}

		/*TEST3.B
			test countChars 	
		*/				
		int chars = tree1.countChar(tree1.getRoot());
		if(chars != 18){
			System.out.println("Test:3.B: Failed, expected:18: but was "+chars+":");	
		}	
		else{System.out.println("Test:3.B: Passed");}


		
		/* TEST STRING 1 */
		String string1 = 	"A quick brown fox jumps over the lazy dog\n"
		+"The question of whether a computer can think is no more interesting than the question of whether a submarine can swim Edsger W Dijkstra\n"
		+"The best programs are written so that computing machines can perform\n"
		+"them quickly and so that human beings can understand them clearly A\n"
		+"programmer is ideally an essayist who works with traditional aesthetic\n"
		+"and literary forms as well as mathematical concepts, to communicate\n"
		+"the way that an algorithm works and to convince a reader that the\n"
		+"results will be correct Donald Ervin Knuth Selected Papers on Computer\n"
		+"Science\n";
	}
}
