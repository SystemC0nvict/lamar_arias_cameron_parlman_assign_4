Assuming you are in the same directory as this README 

#CLEAN
run:
ant -buildfile wordTree/src/build.xml clean


_____________________________________________________________________________
#COMPILE
run:
ant -buildfile wordTree/src/build.xml all


_____________________________________________________________________________
#RUN with command line arguments  LOCAL INPUT	
	Note: replace "src/input2.txt src/output.txt" with your input file and a
	output file name. also replace "banana,quick,fox" with words you want to delete. 
run: 
ant -buildfile wordTree/src/build.xml run -Dargs='src/input2.txt src/output.txt 3 banana,quick,fox 0'


_____________________________________________________________________________
#RUN with command line arguments, input via home/"username"/input_file/
	Note: replace cameron with your username... 
run:
ant -buildfile wordTree/src/build.xml run -Dargs='/home/cameron/input_file/dpP4IN1.txt src/output.txt 3 banana,quick,fox 0'


_____________________________________________________________________________
#TEST
run:
ant -buildfile wordTree/src/build.xml test -Darg0=src/input.txt -Darg1=src/output.txt -Darg2=1 -Darg3=banana -Darg4=0

_____________________________________________________________________________
#JAVADOCS
run: 
javadoc -d wordTree/src/BUILD/docs/ -sourcepath wordTree/src/ wordTree.driver wordTree.util wordTree.store wordTree.threadMgmt



_____________________________________________________________________________
#Data Structures 
ArrayList<Thread> keeps track of threads, makes it easier to join threads.
	Running time:Search: O(n)

Binary Search Tree of Nodes. 
	Running time:Search: O(logN) 
	Running time:insert: O(logN)
The BST compares the lexicographical ordering of words. 
the lexicographical ordering of the the tree makes a BST ideal for searching and inserting words.  

_____________________________________________________________________________
#DEBUG VALUES 

DEBUG_VALUE=4 [Print to stdout everytime a constructor is called]
DEBUG_VALUE=3 [Print to stdout everytime a thread's run() method is called]
DEBUG_VALUE=2 [prints out the words in the Tree]
DEBUG_VALUE=1 [prints out method calls from Treebuilder]
DEBUG_VALUE=0 [No output should be printed from the applicatio to stdout. It is ok to write to the output file though" ]



_____________________________________________________________________________
#Notes 
The make file can be used to run most if not all the commands. 




"I have done this assignment completely on my own. I have not copied
it, nor have I given my solution to anyone else. I understand that if
I am involved in plagiarism or cheating I will have to sign an
official form that I have cheated and that this form will be stored in
my official university record. I also understand that I will receive a
grade of 0 for the involved assignment for my first offense and that I
will receive a grade of F for the course for any additional
offense.â€

[Date: 11/7/2017] -- Please add the date here
Cameron Parlman.
Lamar Arias. 


